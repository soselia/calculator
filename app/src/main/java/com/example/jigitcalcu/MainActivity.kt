package com.example.jigitcalcu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView


class MainActivity : AppCompatActivity() {

    private lateinit var inputTxt: TextView
    private lateinit var operator: String
    private var double1:Double = 0.0
    private var double2:Double = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        inputTxt = findViewById(R.id.input)

    }

    fun buttonClick(view: View){
        if(view is Button){
            var inputT = inputTxt.text.toString()
            val button = view.text.toString()

            if(inputT == "0"){
                inputT = ""
            }

            val sum = inputT + button

            inputTxt.text = sum
        }
    }

    fun operationClick(view: View){
        if (view is Button){
            val operator = view.text.toString()
            this.operator = operator
            val inputT = inputTxt.text.toString()

            if(!inputT.isNullOrEmpty()){
                double1 = inputT.toDouble()
            }
            inputTxt.text = ""

        }
    }

    fun equalsClick(view: View){
        val secInput = inputTxt.text.toString()
        var double2 = 0.0

        if(!secInput.isNullOrEmpty()){
            double2 = secInput.toDouble()
            this.double2 = double2
        }

        when(operator){
            "+" -> inputTxt.text = (double1 + double2).toString()
            "-" -> inputTxt.text = (double1 - double2).toString()
            "/" -> inputTxt.text = (double1 / double2).toString()
            "*" -> inputTxt.text = (double1 * double2).toString()
            "%" -> inputTxt.text = (double1 * double2*0.01).toString()
            "√" -> inputTxt.text = Math.sqrt(double1).toString()
        }

    }

    fun mpClick(view: View){
        double1 = inputTxt.text.toString().toDouble()

        inputTxt.text = (0-double1).toString()

        double1 *= -1


    }

    fun clearClick(view: View){
        inputTxt.text = "0"
    }

    fun dotClick(view: View){
        val plusDot = inputTxt.text.toString()

        if(!inputTxt.text.isNullOrEmpty()){
            inputTxt.text = plusDot + "."
        }
    }
}
/////